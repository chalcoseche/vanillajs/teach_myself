setTimeout(()=>{
    document.body.style.backgroundColor = 'red';
},1000)

// 배경색을 1초마다 지연시간 을 주려함
// setTimeout 을 사용

setTimeout(()=>{
    document.body.style.backgroundColor ='orage';
},2000)
setTimeout(()=>{
    document.body.style.backgroundColor ='yellow';
},3000)

// 길어지는거보단 1초후에 1초후에 1초후에 이런식으로 사용 할 수 있음

setTimeout(()=>{
    document.body.style.backgroundColor ='red';
    setTimeout(()=>{
        document.body.style.backgroundColor='blue';
        setTimeout(()=>{
            document.body.style.backgroundColor='green';
            setTimeout(()=>{
                document.body.style.backgroundColor='purple';
                setTimeout(()=>{
                    document.body.style.backgroundColor='black';
                    setTimeout(()=>{
                        document.body.style.backgroundColor='navy';
                    },1000)
                },1000)
            },1000)
        },1000)
    },1000);
},1000);